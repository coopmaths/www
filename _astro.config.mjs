module.exports = {
    // Définit les options par défaut pour tous les composants Astro
    defaultOptions: {
      // Définit la fonction `client:load` par défaut pour tous les composants Astro
      client: {
        load: true
      }
    }
  };