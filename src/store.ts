import { writable } from 'svelte/store'
// pour la gestion du mode sombre
export const darkMode = writable({ isActive: false })