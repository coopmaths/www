export interface Cahier {
  seances: SeancesEntity[]
}
export interface SeancesEntity {
  numero: number
  notions: string[]
  echauffement: string
  exercices: string[]
  test: string
}