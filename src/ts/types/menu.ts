export type MenuEntry = {
  titre: string
  action: string
  blank: boolean
}

export type MenuRoot = {
  titre: string
  id: string
  entrees: MenuEntry[]
  isMenuOpen: boolean
  actionIfEmpty?: string
}