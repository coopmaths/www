import type { MenuRoot } from './types/menu'

export const startingMenus: Record<string, MenuRoot> = {
  mathalea: {
    titre: 'MathALÉA',
    id: 'mathalea',
    entrees: [],
    isMenuOpen: false,
    actionIfEmpty: 'https://coopmaths.fr/alea'
  },
  blog: {
    titre: 'Blog',
    id: 'blog',
    entrees: [],
    isMenuOpen: false,
    actionIfEmpty: '/www/blog'
  },
  statiques: {
    titre: 'Ressources',
    id: 'statiques',
    entrees: [
      { titre: 'Cahiers de vacances', action: '/www/cahiers-de-vacances', blank: false },
      { titre: 'Sixième', action: '/www/6e', blank: false },
      { titre: 'Exerciseurs', action: '/www/exerciseurs/', blank: false },
      { titre: 'ApiGeom', action: 'https://coopmaths.fr/apigeom', blank: true }
    ],
    isMenuOpen: false
  },
  documentations: {
    titre: 'Documentations',
    id: 'documentations',
    entrees: [
      { titre: 'Utiliser MathALÉA', action: 'https://coopmaths.fr/mathalea_tuto/', blank: false },
      {
        titre: 'Programmer des figures géométriques',
        action: 'https://coopmaths.fr/docMathalea2d/presentation/',
        blank: false
      },
      {
        titre: 'Animations avec des instruments de géométrie',
        action: 'https://coopmaths.fr/docMathalea2d/InstrumEnPoche/',
        blank: false
      },
      {
        titre: 'Documentation pour les développeurs',
        action: 'https://coopmaths.fr/documentation',
        blank: false
      }
    ],
    isMenuOpen: false
  },
  outils: {
    titre: 'Outils',
    id: 'outils',
    entrees: [
      {
        titre: 'Programmation de figures géométriques',
        action: 'https://coopmaths.fr/mathalea2d.html',
        blank: false
      },
      {
        titre: 'Animations avec des instruments de géométrie',
        action: 'https://coopmaths.fr/mathalea2iep.html',
        blank: false
      },
      { titre: 'Divers', action: 'https://coopmaths.fr/mathalea.html?filtre=outils', blank: false }
    ],
    isMenuOpen: false
  },
  aPropos: {
    titre: 'À Propos',
    id: 'apropos',
    entrees: [
      { titre: 'À propos', action: '/www/about', blank: false },
      { titre: 'Partenariats', action: '/www/partenariats', blank: false },
      { titre: 'Contribuer ou aider', action: '/www/contribuer', blank: false },
      {
        titre: 'Vidéos - PodEduc',
        action: 'https://podeduc.apps.education.fr/coopmaths/',
        blank: false
      },
      { titre: 'Mentions légales', action: '/www/mentions_legales', blank: false },
      { titre: 'Nous contacter', action: 'mailto:contact@coopmaths.fr', blank: false }
    ],
    isMenuOpen: false
  }
}
