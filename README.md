# Coopmaths.fr

La nouvelle partie statique du site **coopmaths.fr** va être développée et partagée ici.

## Tester en local

Après avoir récupéré le code de ce dépot :

```console
pnpm install
pnpm start
```

## Modifier les pages

Les pages sont présentes dans `src/pages` elles peuvent être en markdown (.md) ou en markdown + component (.mdx).
Dans ce deuxième cas, il est possible d'importer des components Svelte.

La mise en page est gérée dans `src/layout`.

Les billets de blog sont dans `src/content/blog` est sont classés par date en s'appuyant sur la ligne `date:` du frontmatter de l'article.

## Code HTML

- Utiliser <Image> pour une image responsive
- Pour créer un lien externe : 

```html
<a href="http..." target="_blank">Texte</a>
```